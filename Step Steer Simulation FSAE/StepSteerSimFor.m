%% Dylan Gabriel
clc, clear , clear figures

%% Car Setup
m = 250;                            % Car weight (kg)
L = 60.25/39.37;                    % Wheelbase (m)
tf = 47/39.37;                      % Front track width (m)
tr = 47/39.37;                      % Rear track width (m)
wdf = .45;                          % Weight dist front
adf = .42;                          % Center of Pressure dist front
df = 112;                           % Total downforce at 35mph (lbf)
lift = @(V)...                      % Function for amount of downforce (N)
    ((df*4.448)/(35/2.237)^2)*V^2;     % given a speed (m/s)     
a = L*(1-wdf);                      % Front axle to CG distance (m)
b = L*(wdf);                        % Rear axle to CG distance (m)
cgh = 12/39.37;                     % CG height (m)
Izz = 278549.18*0.00029264;         % Yaw moment of inertia (kg*m^2)
g = 9.81;

%% Inital conditions
Vx = 35/2.237;
Vy(1) = 0;
V(1) = Vx;
r(1) = 0;
theta(1) = 0;
ay(1) = 0;

%% Step Steer input
delta(1) = 10*pi/180;        % Steer angle inital (rad)
dt = .0001;                  % Timestep
time = 0:dt:3;               % Time Vector
i = 1;                       % Index

for i = 1:length(time)
    % Slip angle calculation (radians)
    alphaFO(i) = atan((Vy(i) + a*r(i))/(Vx + tf/2*r(i))) - delta(i);
    alphaFI(i) = atan((Vy(i) + a*r(i))/(Vx - tf/2*r(i))) - delta(i);
    alphaRO(i) = atan((Vy(i) - b*r(i))/(Vx + tr/2*r(i)));
    alphaRI(i) = atan((Vy(i) - b*r(i))/(Vx - tr/2*r(i)));
    % Static weight on each wheel (N)
    wtFO = .5*wdf*m*g + .5*adf*lift(Vx);
    wtFI = .5*wdf*m*g + .5*adf*lift(Vx);
    wtRO = .5*(1-wdf)*m*g + .5*(1-adf)*lift(Vx);
    wtRI = .5*(1-wdf)*m*g + .5*(1-adf)*lift(Vx);
    % Load Transfer on each axle (N)
    LTF(i) = (ay(i)*m*cgh*wdf/tf);
    LTR(i) = (ay(i)*m*cgh*(1-wdf)/tr);
    % Normal load on each tire due to load transfer (N)
    fzFO(i) = wtFO + LTF(i);
    fzFI(i) = wtFI - LTF(i);
    fzRO(i) = wtRO + LTR(i);
    fzRI(i) = wtRI - LTR(i);
    % If a tire lifts
    if fzFI(i) < 0
        fzFO(i) = fzFO(i) - fzFI(i);
        fzFI(i) = 0;
    elseif fzRI(i) < 0
        fzRO(i) = fzRO(i) - fzRI(i);
        fzRI(i) = 0;
    end
    % Lateral forces generated for each tire (N) **pacejka(N,deg)**
    fyFO(i) = pacejka_lat(fzFO(i),alphaFO(i)*180/pi); 
    fyFI(i) = pacejka_lat(fzFI(i),alphaFI(i)*180/pi);
    fyRO(i) = pacejka_lat(fzRO(i),alphaRO(i)*180/pi);
    fyRI(i) = pacejka_lat(fzRI(i),alphaRI(i)*180/pi);
    % Integration of 1st EOM
    rdot(i+1) = (a*(fyFO(i)+fyFI(i))*cos(delta(i)) - b*(fyRO(i)+fyRI(i)))/Izz;
    r(i+1)= 0.5*(rdot(i) + rdot(i+1))*dt + r(i);
    theta(i+1)= 0.5*(r(i) + r(i+1))*dt + theta(i);
    % Integration of 2nd EOM
    Vydot(i+1) = (((fyFO(i)+fyFI(i))*cos(delta(i)) + (fyRO(i)+fyRI(i)))/m) - V(i)*r(i);
    Vy(i+1) = 0.5*(Vydot(i) + Vydot(i+1))*dt + Vy(i);
    V(i+1) = Vx;
    % Resulting Ay
    ay(i+1) = V(i+1)*r(i+1) + Vydot(i+1);
    %% Controller
    delta(i+1) = delta(i);
    
    % Loop stuff
    time(i+1) = time(i) + dt;
end

%% Maximums
[ayMax, ayMaxP] = max(ay);
[rMax, rMaxP] = max(r);
[rdotMax, rdotMaxP] = max(rdot);


%% Plots
figure 
subplot(3,1,1)
hold on
plot(time,ay/9.81)
plot(time(ayMaxP),ayMax/9.81,'*r')
text(time(ayMaxP),ayMax/9.81,sprintf('\n%1.2f g''s',ayMax/9.81))
title('Lateral Acceleration vs Time')
xlabel('time (s)')
ylabel('Lat Accel (g''s)')
xlim([-.05 max(time)+.01])
ylim([0 2.2])
grid on

subplot(3,1,2)
hold on
plot(time,r*180/pi)
plot(time(rMaxP),rMax*180/pi,'*r')
text(time(rMaxP),rMax*180/pi,sprintf('\n%1.2f deg/s',rMax*180/pi))
title('Yaw Rate vs Time')
xlabel('time (s)')
ylabel('Yaw Rate (deg/s)')
xlim([-.05 max(time)+.01])
grid on

subplot(3,1,3)
hold on
plot(time,rdot*180/pi)
plot(time(rdotMaxP),rdotMax*180/pi,'*r')
text(time(rdotMaxP),rdotMax*180/pi,sprintf('\n%1.2f deg/s^2',rdotMax*180/pi))
title('Yaw Accel vs Time')
xlabel('time (s)')
ylabel('Yaw Accel (deg/s^2)')
xlim([-.05 max(time)+.01])
grid on

% subplot(4,1,4)
% hold on
% plot(time,theta*180/pi)
% % plot(time(rdotMaxP),rdotMax*180/pi,'*r')
% % text(time(rdotMaxP),rdotMax*180/pi,sprintf('\n%1.2f deg/s^2',rdotMax*180/pi))
% title('Yaw Pos vs Time')
% xlabel('time (s)')
% ylabel('Theta (deg)')
% xlim([-.05 max(time)+.01])
% grid on

% Slip Angle
% figure
% subplot(2,2,1)
% plot(time(:,1:length(time)-1),alphaFI*180/pi)
% title('Slip angle FI vs time')
% xlabel('Time (s)')
% ylabel('Slip angle (degrees)')
% grid on
% 
% subplot(2,2,2)
% plot(time(:,1:length(time)-1),alphaFO*180/pi)
% title('Slip angle FO vs time')
% xlabel('Time (s)')
% ylabel('Slip angle (degrees)')
% grid on
% 
% subplot(2,2,3)
% plot(time(:,1:length(time)-1),alphaRI*180/pi)
% title('Slip angle RI vs time')
% xlabel('Time (s)')
% ylabel('Slip angle (degrees)')
% grid on
% 
% subplot(2,2,4)
% plot(time(:,1:length(time)-1),alphaRO*180/pi)
% title('Slip angle RO vs time')
% xlabel('Time (s)')
% ylabel('Slip angle (degrees)')
% grid on
% 
% % Tire Fy
% figure
% subplot(2,2,1)
% plot(time(:,1:length(time)-1),fyFI/4.448)
% title('Fy FI vs time')
% xlabel('Time (s)')
% ylabel('Fy (lbs)')
% grid on
% 
% subplot(2,2,2)
% plot(time(:,1:length(time)-1),fyFO/4.448)
% title('Fy FO vs time')
% xlabel('Time (s)')
% ylabel('Fy (lbs)')
% grid on
% 
% subplot(2,2,3)
% plot(time(:,1:length(time)-1),fyRI/4.448)
% title('Fy RI vs time')
% xlabel('Time (s)')
% ylabel('Fy (lbs)')
% grid on
% 
% subplot(2,2,4)
% plot(time(:,1:length(time)-1),fyRO/4.448)
% title('Fy RO vs time')
% xlabel('Time (s)')
% ylabel('Fy (lbs)')
% grid on
% 
% % Tire Fz
% figure
% subplot(2,2,1)
% plot(time(:,1:length(time)-1),fzFI/4.448)
% title('Fz FI vs time')
% xlabel('Time (s)')
% ylabel('Fz (lbs)')
% grid on
% 
% subplot(2,2,2)
% plot(time(:,1:length(time)-1),fzFO/4.448)
% title('Fz FO vs time')
% xlabel('Time (s)')
% ylabel('Fz (lbs)')
% grid on
% 
% subplot(2,2,3)
% plot(time(:,1:length(time)-1),fzRI/4.448)
% title('Fz RI vs time')
% xlabel('Time (s)')
% ylabel('Fz (lbs)')
% grid on
% 
% subplot(2,2,4)
% plot(time(:,1:length(time)-1),fzRO/4.448)
% title('Fz RO vs time')
% xlabel('Time (s)')
% ylabel('Fz (lbs)')
% grid on
% 


