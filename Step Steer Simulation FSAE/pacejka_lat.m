function Y = pacejka_lat(Fz,alpha)
alpha = abs(alpha);
Fz = Fz/4.44822;
mu = .65;
beta = [1.3925,-0.0011,2.5986,61.8209,0.1960,3.6969e+05,-0.0793e-05,...
    0.0007,0.0020,-0.0020,0.1653,-7.7303];
C = beta(1);
c1 = beta(2);
c2 = beta(3);
c3 = beta(4);
c4 = beta(5);
c5 = beta(6);
c6 = beta(7);
c7 = beta(8);
c8 = beta(9);
dE = beta(10);
SH = beta(11);
SV = beta(12);

D = mu.*(c1*Fz.^2+c2*Fz); 
BCD = c3*sind(c4*atand(c5*Fz)); 
B = BCD./(C.*D);
x = alpha + SH;
E = (c6*Fz.^2+c7.*Fz+c8)+dE*sign(x);
y = D.*sind(C.*atand(B.*x-E.*(B.*x-atand(B.*x)))); 
Y0 = y + SV;
Y0(isnan(Y0)) = 0;
if alpha == 0
    Y0 = 0;
end
Y = abs(Y0*4.44822);
% Y = Y0;

end