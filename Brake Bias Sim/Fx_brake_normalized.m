function [ u ] = Fx_brake_normalized( Fz )
% global S;
% str = load('tire_coefficients_Hoosier_10in_LC0_7inRim.mat');
% S = str.tire_coefficients;

% Hoosier R25B IA = 0deg P = 12psi
%  beta =      [    C,      c1,    c2,    c3,    c4,        c5,     c6,     c7,      c8,      dE,    SH,     SV];
beta_10_R25B = [1.2309,-0.0027,2.9719,1.1974,0.0596,4.6389e+04,-0.0140,11.4756,253.5530,138.2941,0.0003,-4.5750]; 
mu = 0.65;
tire = @(Fz,SR) pacejka_fun_93(beta_10_R25B,[Fz SR mu]);

SR = 0:-.01:-.5;
for i = 1:length(SR)
    Fx(i) = tire(Fz,SR(i));
end
u = min(Fx)/Fz;

end
