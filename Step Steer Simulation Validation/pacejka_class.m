function [ fy,Ca ] = pacejka_class( fz,alpha )
% Tire coefficents
a = [0, -22.1, 1011, 1078, 1.82, 0.208, 0, -.354, .707];

E = a(7)*fz^2 + a(8)*fz + a(9);
C = 1.30;
D = a(1)*fz^3 + a(2)*fz^2 + a(3)*fz;
B = (a(4)*sin(a(5)*atan(a(6)*fz)))/(C*D);
Ca = B*C*D;
phi = (1-E)*alpha + (E/B)*atan(B*alpha);

fy = D*sin(C*atan(B*phi));
end

