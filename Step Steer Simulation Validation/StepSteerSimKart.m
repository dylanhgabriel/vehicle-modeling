%% Dylan Gabriel
clc, clear variables, close all
addpath('GPS')
addpath('DATA')
%% Car Setup
m = 1535;                           % Car weight (kg)
wdf = .61;                          % Weight dist front
L = 2.795;                          % Wheelbase (m)
b = wdf*L;                          % Distance from CG to rear axle (m)
a = L - b;                          % Distance from CG to front axle (m)
tf = 1.591;                         % Front Track Width
tr = 1.591;                         % Rear Track Width
cgh = 1.455*.333;                   % CG Height
Izz = 3200;                         % Yaw moment of inertia (kg*m^2)
rack = 13;                          % Steering Rack Ratio
g = 9.81;
%% Measured Data
data = load('Kia_dbl_lane_2');
% GPS ECEF to something actually useful
j=1;
for k=1:length(data.data.septentrio.PositionX)
    if(~isnan(data.data.septentrio.PositionX(k)))
         XYZ(j,:)=[data.data.septentrio.PositionX(k) data.data.septentrio.PositionY(k) data.data.septentrio.PositionZ(k)];
         XYZ_VEL(j,:)=[data.data.septentrio.VelocityX(k) data.data.septentrio.VelocityY(k) data.data.septentrio.VelocityZ(k)];
        [Lat(j),Long(j),Alt(j)]=wgsxyz2lla(XYZ(j,:));
        [ENU(j,:)]=wgsxyz2enu(XYZ(j,:)', Lat(1), Long(1), Alt(1));
        [ENU_VEL(j,:)]=wgsdiffxyz2diffenu(XYZ_VEL(j,:)',Lat(1),Long(1));
        speed_gps(j)=norm(ENU_VEL(j,1:2));
        t_gps(j)=data.data.septentrio.time(k);
        j=j+1;
    end
end
% Match GPS and steer angle sample amount
time = data.data.vehicle.steering.time;
speed_gps1 = interp1(t_gps,speed_gps,time);
speed_gps1(isnan(speed_gps1))=0;
% Cut steering data where manuver starts
startVel = 13;
manuver = find(speed_gps1 > startVel);
speed_gps1 = speed_gps1(manuver);
time = data.data.vehicle.steering.time(manuver);
wheelAngle = data.data.vehicle.steering.Angle(manuver);
% match GPS and IMU sample amount
t2 = data.data.xbow.time;
speed_gps2 = interp1(t_gps,speed_gps,t2);
speed_gps2(isnan(speed_gps2))=0;
% Cut IMU data where manuver starts
manuver = find(speed_gps2 > startVel);
rData = -data.data.xbow.yawRate(manuver);
ayData = data.data.xbow.aY(manuver);
t2 = data.data.xbow.time(manuver);
%% Inital conditions
Vx = speed_gps1;
Vy(1) = 0;
V(1) = Vx(1);
r(1) = 0;
theta(1) = 0;
ay(1) = 0;
delta = wheelAngle/rack;   % Steer angle inital (rad)
dt = time(2) - time(1);      % Timestep of steer input
%% Sim
for i = 1:length(time)-1
    % Slip angle calculation (radians)
    alphaFO(i) = atan((Vy(i) + a*r(i))/(Vx(i) + tf/2*r(i))) - delta(i);
    alphaFI(i) = atan((Vy(i) + a*r(i))/(Vx(i) - tf/2*r(i))) - delta(i);
    alphaRO(i) = atan((Vy(i) - b*r(i))/(Vx(i) + tr/2*r(i)));
    alphaRI(i) = atan((Vy(i) - b*r(i))/(Vx(i) - tr/2*r(i)));
    % Static weight on each wheel (N)
    wtFO = .5*wdf*m*g;
    wtFI = .5*wdf*m*g;
    wtRO = .5*(1-wdf)*m*g;
    wtRI = .5*(1-wdf)*m*g;
    % Load Transfer on each axle (N)
    LTF(i) = (ay(i)*m*cgh*wdf/tf);
    LTR(i) = (ay(i)*m*cgh*(1-wdf)/tr);
    % Normal load on each tire due to load transfer (N)
    fzFO(i) = wtFO + LTF(i);
    fzFI(i) = wtFI - LTF(i);
    fzRO(i) = wtRO + LTR(i);
    fzRI(i) = wtRI - LTR(i);
    % If a tire lifts
    if fzFI(i) < 0
        fzFO(i) = fzFO(i) - fzFI(i);
        fzFI(i) = 0;
    elseif fzRI(i) < 0
        fzRO(i) = fzRO(i) - fzRI(i);
        fzRI(i) = 0;
    end
    % Lateral forces generated for each tire (N) **pacejka(kN,deg)**
    fyFO(i) = -pacejka_class(fzFO(i)/1000,alphaFO(i)*180/pi); 
    fyFI(i) = -pacejka_class(fzFI(i)/1000,alphaFI(i)*180/pi);
    fyRO(i) = -pacejka_class(fzRO(i)/1000,alphaRO(i)*180/pi);
    fyRI(i) = -pacejka_class(fzRI(i)/1000,alphaRI(i)*180/pi);
    fyFO(isnan(fyFO))=0;
    fyFI(isnan(fyFI))=0;
    fyRO(isnan(fyRO))=0;
    fyRI(isnan(fyRI))=0;
    % Integration of 1st EOM
    rdot(i+1) = (a*(fyFO(i)+fyFI(i))*cos(delta(i)) - b*(fyRO(i)+fyRI(i)))/Izz;
    r(i+1)= 0.5*(rdot(i) + rdot(i+1))*dt + r(i);
    theta(i+1)= 0.5*(r(i) + r(i+1))*dt + theta(i);
    % Integration of 2nd EOM
    Vydot(i+1) = (((fyFO(i)+fyFI(i))*cos(delta(i)) + (fyRO(i)+fyRI(i)))/m) - V(i)*r(i);
    Vy(i+1) = 0.5*(Vydot(i) + Vydot(i+1))*dt + Vy(i);
    V(i+1) = Vx(i);
    % Resulting Ay
    ay(i+1) = V(i+1)*r(i+1) + Vydot(i+1);
end

%% Maximums
[deltaMax, deltaMaxP] = max(delta);
[deltaMin, deltaMinP] = min(delta);
% Sim
[ayMax, ayMaxP] = max(ay);
[ayMin, ayMinP] = min(ay);
[rMax, rMaxP] = max(r);
[rMin, rMinP] = min(r);
% Data
[ayMaxD, ayMaxDP] = max(ayData);
[ayMinD, ayMinDP] = min(ayData);
[rMaxD, rMaxDP] = max(rData);
[rMinD, rMinDP] = min(rData);
%% Steering Plot
figure
subplot(3,1,1)
hold on
% Input
plot(time,delta*180/pi,'k')
% Max
plot(time(deltaMaxP),deltaMax*180/pi,'*k')
text(time(deltaMaxP),deltaMax*180/pi,sprintf('\n%1.2f deg/s',deltaMax*180/pi))
% Min
plot(time(deltaMinP),deltaMin*180/pi,'*k')
text(time(deltaMinP),deltaMin*180/pi,sprintf('\n%1.2f deg/s',deltaMin*180/pi))
% Labels
title('Steer Angle Input vs Time')
xlabel('time (s)')
ylabel('Steer Angle (deg)')
xlim([time(1) max(time)])
grid on
%% Yaw Rate Plot
subplot(3,1,2)
hold on
% Sim
plot(time,r*180/pi)
% Data
plot(t2,rData*180/pi,'r--')
    % Sim Max
plot(time(rMaxP),rMax*180/pi,'*b')
text(time(rMaxP),rMax*180/pi,sprintf('\n%1.2f deg/s',rMax*180/pi),'color','blue')
    % Sim Min
plot(time(rMinP),rMin*180/pi,'*b')
text(time(rMinP),rMin*180/pi,sprintf('\n%1.2f deg/s',rMin*180/pi),'color','blue')
    % Data Max
plot(t2(rMaxDP),rMaxD*180/pi,'*r')
text(t2(rMaxDP),rMaxD*180/pi,sprintf('\n%1.2f deg/s  ',rMaxD*180/pi),'VerticalAlignment','bottom','HorizontalAlignment','right','color','red')
    % Data Min
plot(t2(rMinDP),rMinD*180/pi,'*r')
text(t2(rMinDP),rMinD*180/pi,sprintf('\n%1.2f deg/s  ',rMinD*180/pi),'VerticalAlignment','bottom','HorizontalAlignment','right','color','red')
% Labels
title('Yaw Rate vs Time')
xlabel('time (s)')
ylabel('Yaw Rate (deg/s)')
xlim([time(1) max(time)])
legend('Simulation','Data')
grid on
%% Ay Plot
subplot(3,1,3)
hold on
% Sim
plot(time,ay/9.81)
% Data
plot(t2,ayData/9.81,'r--')
    % Sim Max
plot(time(ayMaxP),ayMax/9.81,'*b')
text(time(ayMaxP),ayMax/9.81,sprintf('\n%1.2f g''s',ayMax/9.81),'color','blue')
    % Sim Min
plot(time(ayMinP),ayMin/9.81,'*b')
text(time(ayMinP),ayMin/9.81,sprintf('\n%1.2f g''s',ayMin/9.81),'color','blue')
    % Data Max
plot(t2(ayMaxDP),ayMaxD/9.81,'*r')
text(t2(ayMaxDP),ayMaxD/9.81,sprintf('\n%1.2f g''s  ',ayMaxD/9.81),'VerticalAlignment','bottom','HorizontalAlignment','right','color','red')
    % Data Min
plot(t2(ayMinDP),ayMinD/9.81,'*r')
text(t2(ayMinDP),ayMinD/9.81,sprintf('\n%1.2f g''s  ',ayMinD/9.81),'VerticalAlignment','bottom','HorizontalAlignment','right','color','red')
% Labels
title('Lateral Acceleration vs Time')
xlabel('time (s)')
ylabel('Lat Accel (g''s)')
xlim([time(1) max(time)])
ylim([-1.25 1.25])
legend('Simulation','Data')
grid on