# Vehicle Modeling

Various Vehicle Dynamics models in MATLAB

Brake Bias Sim
*  First big MATLAB project completed in 2018. Simulates a vehicle braking event to determine the ideal bias of braking torque between the front and rear wheels.
*  Plots brake bias vs decel time where the bias with the lowest decel time is the best. Iterates each bias until vehicle is stable and then compares each result.
*  Uses pacejka tire model

Step Steer Simulation FSAE
*  Transient handling model of a fsae vehicle using nonlinear equations of motion. 
*  Includes effects of downforce and tire limits using Pacejka tire model

Step Steer Simulation Validation
*  Uses same vehicle model as the fsae simulation, but parameters are changed to simulate kia test vehicle
*  Data is taken from vehicle performing a double lane change.
*  Compares the transient sim to measured IMU and GPS data during the manuver. 
*  GPS data is converted from ECEF to ENU to determine vehicle speed.
